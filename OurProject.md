# Principles of Digital Fabrication - Drinking Nemo

521159P: Principles of Digital Fabrication, creating arduino -based machine that has alcohol benzene sensor and reacts to users breath.

## Our project idea in a nutshell

Drinking nemo - a fish that reacts if user has alcohol in their breath. When the power is on and alcohol benzene sensor detects alcohol, Nemo's red LED eyes will brighten and it's wheels start to move the fish. In the end nemo passes out and you can test it again or turn the power off.

## Project description

Our project can spesifically inform you when you have drinked alcohol. We thought that it would have been cool if we could have done an accurate breathalyzer on the project that tells you when you drank too much to drive. However, we did not know how accurate our sensor is.

There are a lot of breathalyzer projects on Arduino on the internet for sure. But we found zero breathalyzer projects that had a physical movement after sensor receiving a breath with alcohol in it. So we really have to make an effort to make the project work the way we want it to. Also, we are going to make the coolest looking breathalyzer project that exist ;)

## Important links to our project

[See how our 3D model is looking right now](uploads/7251d3d0af8869ded1db83a2081fac3d/image.png)

[TinkerCad Circuit of the Drinking Nemo](https://www.tinkercad.com/things/5u90ofnPUws-drinking-nemostart/editel?sharecode=wtMZVFfQcYafP8gaby7oblpe0u9oPfECzpbRN44cDUM)


## Tools we are using besides GitLab

- TinkerCad
- Fusion 360
- InkScape

## Documentation

- [Weekly reports on DigiFab](https://www.digifab-oulu.com/)
- Repository etc on Wiki/GitLab


## Our project goals

- [ ] To make the project work the way as we want to
- [ ] TinkerCad model for Circuit
- [ ] 3D model to the body of our fish
- [ ] Combining all our electronic components
- [ ] Combining all the physical parts togehter 
- [ ] Making of the code
- [ ] Testing our project and making sure everything works
 
## Visuals / Pictures

You can find pictures and videos of our project on our Wiki page in GitLab :) There's a cool video of us using laser cutter ;)

## The sustainability of our project

We try to be extremely careful to make our project as sustainable as possible. For example, cutting all the parts as close to the edge as possible and measuring everything carefully so we don't have to cut or print anything too many times. We first thought that we would have purchased tires for the project from a flea market, but they would have been difficult to find and have not been made in FabLab...

## How to use our gadget:

1. Turn the power on
2. Blow towards the alcohol benzene
3. If the eyes turn red and Nemo starts moving, you shoulnd't drive
4. Watch the show
5. Wait for Nemo stop moving
6. Blow towards the breathalyzer again or turn off the amazing gadget you just used

## Some thoughts

We believe this will be one of the coolest project on this course if everything turns out the way we want to



